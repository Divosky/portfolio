<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class PageController extends Controller
{
    public function start()
    {
      return view('pages.start');
    }
    
    public function projects()
    {
      return view('pages.projects');
    }

    public function contact()
    {
      return view('pages.contact');
    }
}
