<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Projects Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during displaying the page with
    | my projects
    |
    */

    'mainTitle' => 'Zrealizowane Projekty',
    'preview' => 'Podgląd',

];
