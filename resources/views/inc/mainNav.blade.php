<nav class="nav-main">
  <ul>
    <li {{ Request::is('/') ? ' class=active' : null }}><a href="/">@lang('nav.homepage')</a></li>
    <li {{ Request::is('projects') ? ' class=active' : null }}><a href="/projects">@lang('nav.projects')</a></li>
    <li {{ Request::is('contact') ? ' class=active' : null }}><a href="/contact">@lang('nav.contact')</a></li>
  </ul>
</nav>