<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>@lang('main.websiteTitle')@yield('pageTitle')</title>

  <link rel="stylesheet" href="{{ asset('css/app.css') }}">

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#cc6666">
  <meta name="theme-color" content="#cc6666">
</head>

<body>
  <div class="app">
    <header class="site-header">
      <section class="whoami">
        <hrgroup>
          <h1 class="whoami-name">Divosky</h1>
          <h3 class="whoami-desc">Front-End Developer</h3>
        </hrgroup>
      </section>
      @include('inc.mainNav')

      <footer class="footer">
        &copy; Mikołaj <i>"Divosky"</i> Szczepański {{ date("Y") }}
      </footer>
    </header>

    <main class="site-content">
      @yield('content')
    </main>
  </div>
</body>
</html>