@extends('layouts.app')

@section('pageTitle', '&nbsp;&raquo;&nbsp;'. __('nav.contact'))

@section('content')
  <div class="boxes contact">
    <section class="box">
      <h3 class="box-title">E-mail</h3>
      <article class="box-content">
        <p><a href="mailto://divoskyy@gmail.com">Divoskyy@gmail.com</a></p>
      </article>
    </section>
    
    <section class="box">
      <h3 class="box-title">Facebook</h3>
      <article class="box-content">
        <p><a href="https://fb.com/divoskyy" rel="nofollow">/divoskyy</a></p>
      </article>
    </section>

    <section class="box">
      <h3 class="box-title">Telegram</h3>
      <article class="box-content">
        <p><a href="https://t.me/Divosky" rel="nofollow">@Divosky</a></p>
      </article>
    </section>

    <section class="box">
      <h3 class="box-title">Discord</h3>
      <article class="box-content">
        <p><a href="https://discord.gg/3PgEqdW" rel="nofollow">Divosky#4078</a></p>
      </article>
    </section>
  </div>
@endsection
