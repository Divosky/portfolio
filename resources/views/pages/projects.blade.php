@extends('layouts.app')

@section('pageTitle', '&nbsp;&raquo;&nbsp;'. __('nav.projects'))

@section('content')
<div class="boxes projects">
  <section class="box">
    <fieldset class="fieldset">
      <legend class="fieldset-title">
        <h3 class="box-title">@lang('projects.mainTitle')<span class="smile"></span></h3>
      </legend>
      <div class="fieldset-content">
        <article class="box-content">
          <div class="project-item">
            <a class="project-link" href="images/projects/1.png">
              <img class="project-image" src="{{URL::asset('images/projects/small-1.png')}}" alt="Project">
              <span class="project-preview">@lang('projects.preview')</span>
            </a>
          </div>
          <div class="project-item">
            <a class="project-link" href="images/projects/2.png">
              <img class="project-image" src="{{URL::asset('images/projects/small-2.png')}}" alt="Project">
              <span class="project-preview">@lang('projects.preview')</span>
            </a>
          </div>
          <div class="project-item">
            <a class="project-link" href="images/projects/3.png">
              <img class="project-image" src="{{URL::asset('images/projects/small-3.png')}}" alt="Project">
              <span class="project-preview">@lang('projects.preview')</span>
            </a>
          </div>
          <div class="project-item">
            <a class="project-link" href="images/projects/4.png">
              <img class="project-image" src="{{URL::asset('images/projects/small-4.png')}}" alt="Project">
              <span class="project-preview">@lang('projects.preview')</span>
            </a>
          </div>
          <div class="project-item">
            <a class="project-link" href="images/projects/5.png">
              <img class="project-image" src="{{URL::asset('images/projects/small-5.png')}}" alt="Project">
              <span class="project-preview">@lang('projects.preview')</span>
            </a>
          </div>
          <div class="project-item">
            <a class="project-link" href="images/projects/6.png">
              <img class="project-image" src="{{URL::asset('images/projects/small-6.png')}}" alt="Project">
              <span class="project-preview">@lang('projects.preview')</span>
            </a>
          </div>
        </article>
      </div>
    </fieldset>
  </section>
</div>
@endsection
