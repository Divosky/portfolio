@extends('layouts.app')

@section('content')
  <section id="start" class="box">
    <h3 class="box-title">@lang('main.mainTitle')<span class="smile"></span></h3>
    <article class="box-content">
      <p>
      @lang('main.article')
      </p>
    </article>
  </section>
@endsection
